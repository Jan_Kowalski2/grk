function setup() {
  createCanvas(512, 512);
  background(255);
}

let x0 = -1;
let y0 = -1;
let x1 = -1;
let y1 = -1;

function mousePressed() {
  x0 = mouseX;
  y0 = mouseY;
}

function mouseDragged() {
  x1 = mouseX;
  y1 = mouseY;
  background(255);
  noStroke();
  fill("red");
  ellipse(x0 - 3, y0 - 3, 6);
  fill("green");
  ellipse(x1 - 3, y1 - 3, 6);
}

function mouseReleased() {
  background(255);
  loadPixels();
  drawLine();
  updatePixels();
}

function setPixel(x, y, c) {
  set(x, y, 0);
}

function drawLine() {
  dx = x1 - x0;
  dy = y1 - y0;
  a = dy / dx;
  b = y0 - a * x0;

  if (x0 < x1)
    for (x = x0; x <= x1; x++) {
      y = a * x + b;
      setPixel(Math.floor(x), Math.round(y), 0);
    }
  else
    for (x = x1; x <= x0; x++) {
      y = a * x + b;
      setPixel(Math.floor(x), Math.round(y), 0);
    }

  updatePixels();
}
