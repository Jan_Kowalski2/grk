function setup() {
  createCanvas(512, 512);
  background(255);
}

let x0 = -1;
let y0 = -1;
let x1 = -1;
let y1 = -1;

function mousePressed() {
  x0 = mouseX;
  y0 = mouseY;
}

function mouseDragged() {
  x1 = mouseX;
  y1 = mouseY;
  background(255);
  noStroke();
  fill("red");
  ellipse(x0 - 3, y0 - 3, 6);
  fill("green");
  ellipse(x1 - 3, y1 - 3, 6);
}

function mouseReleased() {
  background(255);
  loadPixels();
  draw_line();
  updatePixels();
}

function set_pixel(x, y, c) {
  set(x, y, 0);
}

function draw_line() {
  dx = x1 - x0;
  dy = y1 - y0;
  a = dy / dx;
  b = y0 - a * x0;
  dinc = 2 * dy - 2 * dx;

  yp = y0;
  dp = 2 * dy - dx;
  deq = 2 * dy;
  d = a - 1 / 2;
  if (x0 < x1)
    for (x = x0; x <= x1; x++) {
      y = a * x + b;
      d += yp === y ? deq : dinc;
      yp = y;
      if (d < 0) set_pixel(Math.floor(x + 1), Math.round(y), 0);
      else set_pixel(Math.floor(x + 1), Math.round(y + 1), 0);
    }
  else
    for (x = x1; x <= x0; x++) {
      y = a * x + b;
      d += yp === y ? deq : dinc;
      yp = y;
      if (d < 0) set_pixel(Math.floor(x + 1), Math.round(y), 0);
      else set_pixel(Math.floor(x + 1), Math.round(y + 1), 0);
    }

  updatePixels();
}
