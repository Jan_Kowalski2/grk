function setup() {
  createCanvas(512, 512);
  background(255);
}

let lastX = -1;
let lastY = -1;

function mouseDragged() {
  if (mouseButton !== LEFT) {
    return;
  }
  if (lastX > 0) {
    line(lastX, lastY, mouseX, mouseY);
  }
  lastX = mouseX;
  lastY = mouseY;
}
function mouseReleased() {
  lastX = lastY = -1;
  if (mouseButton === RIGHT) {
    loadPixels();
    fillFlood(mouseX, mouseY);
    updatePixels();
  }
}

function getPixel(x, y) {
  idx = (y * 512 + x) * 4;
  return pixels[idx];
}

coloredPixels = [];
function fillFlood(x, y) {
  coloredPixels.push([x, y]);
  
  while (coloredPixels.length > 0) {
    [x1, y1] = coloredPixels.pop();

    if (x1 >= 0 && x1 <= 512 && y1 >= 0 && y1 <= 512) {
      pixel = getPixel(Math.floor(x1), Math.floor(y1));
      if (pixel === 255) {
        set(Math.floor(x1), Math.floor(y1), color(200));
        if (getPixel(Math.floor(x1), Math.floor(y1 - 1)) === 255)
          coloredPixels.push([x1, y1 - 1]);
        if (getPixel(Math.floor(x1), Math.floor(y1 + 1)) === 255)
          coloredPixels.push([x1, y1 + 1]);
        if (getPixel(Math.floor(x1 - 1), Math.floor(y1)) === 255)
          coloredPixels.push([x1 - 1, y1]);
        if (getPixel(Math.floor(x1 + 1), Math.floor(y1)) === 255)
          coloredPixels.push([x1 + 1, y1]);
      }
    }
  }
}
