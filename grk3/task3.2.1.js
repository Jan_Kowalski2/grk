function setup() {
  createCanvas(512, 512);
  background(255);
}

let x0 = -1;
let y0 = -1;
let x1 = -1;
let y1 = -1;

function mousePressed() {
  x0 = mouseX;
  y0 = mouseY;
}

function mouseDragged() {
  x1 = mouseX;
  y1 = mouseY;
  background(255);
  noStroke();
  fill("red");
  ellipse(x0 - 3, y0 - 3, 6);
  fill("green");
  ellipse(x1 - 3, y1 - 3, 6);
}

function mouseReleased() {
  background(255);
  loadPixels();
  drawLine();
  updatePixels();
}

function setPixel(x, y, c) {
  set(x, y, color(-c, c, 0));
  idx = (y * 512 + x) * 4;
}

function drawLine() {
  dx = Math.abs(x0 - x1);
  dy = y1 - y0;
  for (x = 0; x < 512; x++)
    for (y = 0; y < 512; y++) {
      dxy = (dy / dx) * (x - x0) - (y - y0);
      setPixel(Math.floor(x), Math.round(y), dxy);
    }
  updatePixels();
}
