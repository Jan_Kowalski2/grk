function setup() {
  createCanvas(512, 512);
  background(255);
}

let x0 = -1;
let y0 = -1;
let x1 = -1;
let y1 = -1;

function mousePressed() {
  x0 = mouseX;
  y0 = mouseY;
}

function mouseDragged() {
  x1 = mouseX;
  y1 = mouseY;
  background(255);
  noStroke();
  fill("red");
  ellipse(x0 - 3, y0 - 3, 6);
  fill("green");
  ellipse(x1 - 3, y1 - 3, 6);
}

function mouseReleased() {
  background(255);
  loadPixels();
  drawLine();
  updatePixels();
}

function set_pixel(x, y, c) {
  set(x, y, 0);
}

function drawLine() {
  dx = x1 - x0;
  dy = y1 - y0;

  if (dx > dy) {
    yi = 1;
    if (dy < 0) {
      yi = -1;
      dy = -dy;
    }
    D = 2 * dy - dx;
    y = y0;
    for (x = x0; x <= x1; x++) {
      set_pixel(x, y);
      if (D > 0) {
        y = y + yi;
        D = D - 2 * dx;
      }
      D = D + 2 * dy;
    }
  } else {
    xi = 1;
    if (dx < 0) {
      xi = -1;
      dx = -dx;
    }
    D = 2 * dx - dy;
    x = x0;

    for (y = y0; y < y1; y++) {
      set_pixel(x, y);
      if (D > 0) {
        x = x + xi;
        D = D - 2 * dy;
      }
      D = D + 2 * dx;
    }
  }
  updatePixels();
}
