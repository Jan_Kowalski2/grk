function makeVektor(x, y) {
  return (tab = [x, y, 1]);
}

function drawVector(img, vec) {
  img.set(vec[0], vec[1], 0);
  img.updatePixels();
}

var initValues = {
  makeTranslationTx: 0,
  makeTranslationTy: 0,
  makeRotationO: 0,
  makeScaleSx: 1,
  makeScaleSy: 1,
  makeShearShx: 0,
	makeShearShy: 0,
	l: [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
};

window.makeTranslationTx = 0;
window.makeTranslationTy = 0;
window.makeRotationO = 0;
window.makeScaleSx = 1;
window.makeScaleSy = 1;
window.makeShearShx = 0;
window.makeShearShy = 0;
window.l = [[1, 0, 0], [0, 1, 0], [0, 0, 1]];

function mouseDragged() {
  vec = makeVektor(mouseX, mouseY);
  drawVector(imgA, vec);
  m = multiplyMatrix(
    makeTranslation(window.makeTranslationTx, window.makeTranslationTy),
    makeRotation(makeRotationO)
  );
  k = multiplyMatrix(m, makeScale(window.makeScaleSx, window.makeScaleSy));
  window.l = multiplyMatrix(
    k,
    makeShear(window.makeShearShx, window.makeShearShy)
  );
  vecB = multiply(l, vec);
  drawVector(imgB, vecB);
  // console.log(vec);
  // console.log(vecB);
  // console.log(l)
  refreshMatrix();
}

function makeIdentity() {
  const tab = [];
  tab[0] = [1, 0, 0];
  tab[1] = [0, 1, 0];
  tab[2] = [0, 0, 1];
  return tab;
}

function makeTranslation(tx = 0, ty = 0) {
  const tab = [];
  tab[0] = [1, 0, tx];
  tab[1] = [0, 1, ty];
  tab[2] = [0, 0, 1];
  return tab;
}

function makeScale(sx = 0, sy = 0) {
  const tab = [];
  tab[0] = [sx, 0, 0];
  tab[1] = [0, sy, 0];
  tab[2] = [0, 0, 1];
  return tab;
}

function makeRotation(Θ = 0) {
  Θ = (Θ / 180) * Math.PI;
  const tab = [];
  tab[0] = [Math.cos(Θ), -Math.sin(Θ), 0];
  tab[1] = [Math.sin(Θ), Math.cos(Θ), 0];
  tab[2] = [0, 0, 1];
  return tab;
}

function makeShear(Shx = 0, Shy = 0) {
  const tab = [];
  tab[0] = [1, Shx, 0];
  tab[1] = [Shy, 1, 0];
  tab[2] = [0, 0, 1];
  return tab;
}

function multiply(tab, vec) {
  const result = [];
  result[0] = tab[0][0] * vec[0] + tab[0][1] * vec[1] + tab[0][2] * vec[2];
  result[1] = tab[1][0] * vec[0] + tab[1][1] * vec[1] + tab[1][2] * vec[2];
  result[2] = tab[2][0] * vec[0] + tab[2][1] * vec[1] + tab[2][2] * vec[2];
  return result;
}

function multiplyMatrix(a, b) {
  var aNumRows = a.length,
    aNumCols = a[0].length,
    bNumRows = b.length,
    bNumCols = b[0].length,
    m = new Array(aNumRows);
  for (var r = 0; r < aNumRows; ++r) {
    m[r] = new Array(bNumCols);
    for (var c = 0; c < bNumCols; ++c) {
      m[r][c] = 0;
      for (var i = 0; i < aNumCols; ++i) {
        m[r][c] += a[r][i] * b[i][c];
      }
    }
  }
  return m;
}

function refreshMatrix() {
  document.getElementById("matrixFirstRow").textContent = window.l[0].join(",");
  document.getElementById("matrixSecondRow").textContent = window.l[1].join(
    ","
  );
  document.getElementById("matrixLastRow").textContent = window.l[2].join(",");
}

function readValueFromInput(inputId) {
  document.getElementById(inputId).addEventListener("keyup", event => {
    window[inputId] = event.target.value;
    refreshMatrix();
  });
}

const resetValues = e => {
	e.preventDefault();
	Object.keys(initValues).forEach(key => {
		window[key] = initValues[key];
	})

	refreshMatrix();
}

var arr = [
  "makeTranslationTx",
  "makeTranslationTy",
  "makeScaleSx",
  "makeScaleSy",
  "makeRotationO",
  "makeShearShx",
  "makeShearShy"
];

document.getElementById("reset").addEventListener("click", resetValues);
arr.forEach(el => readValueFromInput(el));
