function makeVektor(x, y){
	return tab=[x,y,1];
}

function drawVector(img, vec){
	img.set(vec[0],vec[1],0);
    img.updatePixels();
}

function mouseDragged(){
	vec = makeVektor(mouseX, mouseY);
    drawVector(imgA, vec);
    m = multiplyMatrix(makeTranslation(50, 50), makeRotation(90));
    k = multiplyMatrix(m, makeScale(2,2))
    vecB = multiply(k, vec);
    drawVector(imgB, vecB);
    console.log(vec);
    console.log(vecB);  
}
  
function makeIdentity(){
	const tab = [];
	tab[0] = [1,0,0];
	tab[1] = [0,1,0];
	tab[2] = [0,0,1];
	return tab;
}

function makeTranslation(tx,ty){
	const tab = [];
	tab[0] = [1,0,tx];
	tab[1] = [0,1,ty];
	tab[2] = [0,0,1];
	return tab;
}

function makeScale(sx,sy){
	const tab = [];
	tab[0] = [sx,0,0];
	tab[1] = [0,sy,0];
	tab[2] = [0,0,1];
	return tab;
}

function makeRotation(Θ){
	Θ = Θ/180 * Math.PI;
	const tab = [];
	tab[0] = [Math.cos(Θ),-Math.sin(Θ),0];
	tab[1] = [Math.sin(Θ),Math.cos(Θ),0];
	tab[2] = [0,0,1];
	return tab;
}

function makeShear(Shx, Shy){
	const tab = [];
	tab[0] = [1,Shx,0];
	tab[1] = [Shy,1,0];
	tab[2] = [0,0,1];
	return tab;
}
  
function multiply(tab, vec){
  	const result = [];
    result[0] = (tab[0][0]*vec[0])+(tab[0][1]*vec[1])+(tab[0][2]*vec[2]);
    result[1] = (tab[1][0]*vec[0])+(tab[1][1]*vec[1])+(tab[1][2]*vec[2]);
    result[2] = (tab[2][0]*vec[0])+(tab[2][1]*vec[1])+(tab[2][2]*vec[2]);
	return result;
}

 function multiplyMatrix(a, b){
	var aNumRows = a.length, aNumCols = a[0].length,
      bNumRows = b.length, bNumCols = b[0].length,
      m = new Array(aNumRows);  
  for (var r = 0; r < aNumRows; ++r) {
    m[r] = new Array(bNumCols);
    for (var c = 0; c < bNumCols; ++c) {
      m[r][c] = 0;             
      for (var i = 0; i < aNumCols; ++i) {
        m[r][c] += a[r][i] * b[i][c];
      }
    }
  }
  return m;
}

console.log(makeIdentity());
console.log(makeTranslation(2,2));
console.log(makeScale(2,2));
console.log(makeRotation(30));
console.log(makeShear(2,2));  
console.log(multiplyMatrix([[1,2,3], [4,5,6], [7,8,9]], makeIdentity()));  