function makeVektor(x, y){
	return tab=[x,y,1];
}

function drawVector(img, vec){
	img.set(vec[0],vec[1],0);
    img.updatePixels();
}

function mouseDragged(){
	vec = makeVektor(mouseX, mouseY);
    drawVector(imgA, vec);
    vecB = multiply(makeTranslation(50,50), vec);
    drawVector(imgB, vecB);
    console.log(vec);
    console.log(vecB);  
}
  
function makeIdentity(){
	const tab = [];
	tab[0] = [1,0,0];
	tab[1] = [0,1,0];
	tab[2] = [0,0,1];
	return tab;
}

function makeTranslation(tx,ty){
	const tab = [];
	tab[0] = [1,0,tx];
	tab[1] = [0,1,ty];
	tab[2] = [0,0,1];
	return tab;
}

function makeScale(sx,sy){
	const tab = [];
	tab[0] = [sx,0,0];
	tab[1] = [0,sy,0];
	tab[2] = [0,0,1];
	return tab;
}

function makeRotation(Θ){
	Θ = Θ/180 * Math.PI;
	const tab = [];
	tab[0] = [Math.cos(Θ),-Math.sin(Θ),0];
	tab[1] = [Math.sin(Θ),Math.cos(Θ),0];
	tab[2] = [0,0,1];
	return tab;
}

function makeShear(Shx, Shy){
	const tab = [];
	tab[0] = [1,Shx,0];
	tab[1] = [Shy,1,0];
	tab[2] = [0,0,1];
	return tab;
}
  
function multiply(tab, vec){
  	const result = [];
    result[0] = (tab[0][0]*vec[0])+(tab[0][1]*vec[1])+(tab[0][2]*vec[2]);
    result[1] = (tab[1][0]*vec[0])+(tab[1][1]*vec[1])+(tab[1][2]*vec[2]);
    result[2] = (tab[2][0]*vec[0])+(tab[2][1]*vec[1])+(tab[2][2]*vec[2]);
	return result;
}

console.log(makeIdentity());
console.log(makeTranslation(2,2));
console.log(makeScale(2,2));
console.log(makeRotation(30));
console.log(makeShear(2,2));  