function preload() {
    img = loadImage(imageSrc);
    img_r = createImage(256, 256);
    img_g = createImage(256, 256);
    img_b = createImage(256, 256);
    img_sum = createImage(256, 256);
}
function setup() {
    createCanvas(512, 512);
    img.resize(256, 256);

    img.loadPixels();
    img_r.loadPixels();
    img_g.loadPixels();
    img_b.loadPixels();

    for (x = 0; x < img.width; x++) {
        for (y = 0; y < img.height; y++) {
            pos = 4 * (y * img.width + x);
            let r = img.pixels[pos]
            let g = img.pixels[pos + 1]
            let b = img.pixels[pos + 2]
            let a = img.pixels[pos + 3]

            img_r.pixels[pos] = r;
            img_g.pixels[pos + 1] = g;
            img_b.pixels[pos + 2] = b;

            img_r.pixels[pos + 3] = 255;
            img_g.pixels[pos + 3] = 255;
            img_b.pixels[pos + 3] = 255;
        }
    }

    img_r.updatePixels();
    img_g.updatePixels();
    img_b.updatePixels();

    image(img_r, 0, 0);
    image(img_g, 0, 256);
    image(img_b, 256, 0);

    img_sum.blend(img_r, 0, 0, 256, 256, 0, 0, 256, 256, ADD);
    img_sum.blend(img_g, 0, 0, 256, 256, 0, 0, 256, 256, ADD);
    img_sum.blend(img_b, 0, 0, 256, 256, 0, 0, 256, 256, ADD);

    image(img_sum, 256, 256);
}