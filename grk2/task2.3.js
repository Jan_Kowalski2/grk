function preload() {
    img = loadImage(imageSrc);
}
function setup() {
    var dimX = 256;
    var dimY = 256;
    createCanvas(dimX, 512);
    img.resize(dimX, dimY);
    img.filter('gray');
    img.loadPixels();
    background('white');

    var histogram = new Array(dimX).fill(0);

    for (x = 0; x < img.width; x++) {
        for (y = 0; y < img.height; y++) {
            pos = 4 * (y * img.width + x);
            var value = img.pixels[pos + 2]
            histogram[value]++;
        }
    }

    var max = Math.max.apply(null, histogram);
    var scaleY = max / dimY; 
    scaleY = 3; // wychodzi 30, ale 1 wartość <czarny> jest bardzo duża.

    for (z = 0; z < dimX; z++) {
        line(z, dimY, z, dimY - (histogram[z] / scaleY));
    }
    
    stroke(0);
    image(img, 0, 256);

}